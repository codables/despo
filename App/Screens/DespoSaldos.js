import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from '../constant/Icon';
import { Avatar, Divider, Snackbar } from 'react-native-paper';


export default class DespoSaldos extends React.Component {



  render() {

    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        <View style={styles.HeaderMain}>
            <Image source={require('../assets/images/orange1.png')} style={styles.HeaderImage}/>
            <Text style={styles.HeaderText}>DESPO</Text>
        </View>
        <ScrollView style={styles.ScrollView} showsVerticalScrollIndicator={false}>
            <View style={styles.Main}>
                <View style={styles.View1Main}>
                  <View style={styles.View1}>
                    <View style={styles.View1ItemMain}>
                        <View style={styles.View1TextIconMain}>
                            <Image source={require('../assets/images/Vector2.png')}/>
                            <Text style={styles.SaldoText}>Saldo 1</Text>
                        </View>
                        <View style={styles.View1TextIconMain}>
                            <Text style={styles.RsPRice}>R$ 600,00</Text>
                            <TouchableOpacity style={styles.CrossIconTouchable}>
                                <Image source={require('../assets/images/Vector3.png')} style={styles.CrossIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.View1ItemMain}>
                        <View style={styles.View1TextIconMain}>
                            <Image source={require('../assets/images/Vector2.png')}/>
                            <Text style={styles.SaldoText}>Saldo 1</Text>
                        </View>
                        <View style={styles.View1TextIconMain}>
                            <Text style={styles.RsPRice}>R$ 600,00</Text>
                            <TouchableOpacity style={styles.CrossIconTouchable}>
                                <Image source={require('../assets/images/Vector3.png')} style={styles.CrossIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.View1ItemMain}>
                        <View style={styles.View1TextIconMain}>
                            <Image source={require('../assets/images/Vector2.png')}/>
                            <Text style={styles.SaldoText}>Saldo 1</Text>
                        </View>
                        <View style={styles.View1TextIconMain}>
                            <Text style={styles.RsPRice}>R$ 600,00</Text>
                            <TouchableOpacity style={styles.CrossIconTouchable}>
                                <Image source={require('../assets/images/Vector3.png')} style={styles.CrossIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.plusIconMain}>
                        <Divider style={styles.Divider}/>
                        <View style={styles.PlusIcon}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoResumo')} style={styles.PlusTouchable}>
                            <Icon name='plus' type='entypo' size={20} style={{fontWeight:'bold'}}/>
                        </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.LancamentoTextMain}>
                        <Text>Lançamento de <Text style={styles.LancamentoText}>Composição de Saldo</Text> </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.MovimentoMainView}>
                    <View style={styles.MovimentoMain}>
                        <Text style={styles.MovimentoText}>Movimento do Caixa</Text>
                        <View style={styles.ItemsView1}>
                            <Text style={styles.ItemsMainHeading}>Plano de Contas</Text>
                            <Text style={styles.ItemsHeadingPrice}>R$2020.40</Text>
                        </View>
                        <View style={styles.ItemsView}>
                            <View style={styles.ItemsSubHeadingMain}>
                                <Icon name='circle' type='fontawesome' size={14} color='#009D70'/>
                                <Text style={styles.ItemsSubHeading}>Conta de Saldo</Text>
                            </View>
                            <Text style={styles.ItemsDate}>Abril 20, 2012 1:54 AM</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.MovimentoMainView1}>
                <View style={styles.MovimentoMain}>
                    <View style={styles.ItemsView2}>
                        <Text style={styles.ItemsMainHeading}>Descricao</Text>
                        <Text style={styles.ItemsHeadingPrice}>R$2020.40</Text>
                    </View>
                    <View style={styles.ItemsView}>
                            <View style={styles.ItemsSubHeadingMain}>
                                <Icon name='circle' type='fontawesome' size={14} color='#009D70'/>
                                <Text style={styles.ItemsSubHeading}>Categoria</Text>
                            </View>
                            <Text style={styles.ItemsDate}>Abril 20, 2012 1:54 AM</Text>
                    </View>
                    </View>
                </View>
                <View style={styles.MovimentoMainView1}>
                <View style={styles.MovimentoMain}>
                    <View style={styles.ItemsView2}>
                        <Text style={styles.ItemsMainHeading}>Descricao</Text>
                        <Text style={styles.ItemsHeadingPrice}>R$2020.40</Text>
                    </View>
                    <View style={styles.ItemsView}>
                            <View style={styles.ItemsSubHeadingMain}>
                                <Icon name='circle' type='fontawesome' size={14} color='#009D70'/>
                                <Text style={styles.ItemsSubHeading}>Categoria</Text>
                            </View>
                            <Text style={styles.ItemsDate}>Abril 20, 2012 1:54 AM</Text>
                    </View>
                    </View>
                </View>
                
                <View style={styles.BottomButtonMain}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.ButtonTouchable1}>
                        <Icon name='cross' type='entypo' color='white' size={22} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.ButtonTouchable2}>
                        <Icon name='check' type='entypo' color='white' size={22} />
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  HeaderMain:{
    backgroundColor:'white',
    width:'100%',
    flexDirection:'row',
    marginTop:20,
    height:60,
    alignItems:'center',
    justifyContent:'center'
  },
  HeaderImage:{
    width:35,
    height:35,
    top:3
  },
  HeaderText:{
    fontSize:16,
    left:5,
    fontWeight:'bold',
    top:3
  },
  ScrollView:{
    width: '100%'
  },
  Main:{
    marginTop:15
  },
  View1Main:{
    backgroundColor:'white',
    height:280,
    width:'100%',
    alignItems:'center'
  },
  View1:{
    backgroundColor:'white',
    width:'95%',
    borderRadius:10,
    marginTop:10,
    height:260,
    elevation:4
  },
  View1ItemMain:{
    marginTop:20,
    left:20,
    width:'88%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  View1TextIconMain:{
    flexDirection:'row',
    alignItems:'center'
  },
  SaldoText:{
    fontSize:16,
    left:10
  },
  RsPRice:{
    fontSize:16
  },
  CrossIconTouchable:{
    left:5
  },
  CrossIcon:{
    width:17,
    height:17
  },
  plusIconMain:{
    marginTop:45
  },
  Divider:{
    padding:1
  },
  PlusIcon:{
    alignSelf:'flex-end',
    marginTop:-27,
    right:20,
  },
  PlusTouchable:{
    backgroundColor:'#FFAC30',
    width:50,
    height:50,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:30
  },
  LancamentoTextMain:{
    alignItems:'center',
    marginTop:20
  },
  LancamentoText:{
    color:'#FFAC30'
  },
  MovimentoMainView:{
    backgroundColor:'white',
    marginTop:15,
    height:120,
    width:'100%'
  },
  MovimentoMain:{
    width:'88%',
    alignSelf:'center',
    marginTop:10
  },
  MovimentoText:{
    fontSize:20,
    color:'#404040'
  },
  ItemsView1:{
    marginTop:15,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  ItemsView:{
    marginTop:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  ItemsMainHeading:{
    fontSize:17,
    color: '#151522'
  },
  ItemsHeadingPrice:{
    fontSize:17,
    color: '#009D70'
  },
  ItemsSubHeadingMain:{
    flexDirection:'row',
    alignItems:'center'
  },
  ItemsSubHeading:{
    fontSize:13,
    color: '#151522',
    left:5
  },
  ItemsDate:{
    fontSize:11,
    color: '#999999'
  },
  MovimentoMainView1:{
    backgroundColor:'white',
    marginTop:2,
    height:80,
    width:'100%'
  },
  ItemsView2:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  BottomButtonMain:{
    marginTop:40,
    marginBottom:30,
    alignItems:'center',
    justifyContent:'space-between',
    flexDirection:'row',
    width:'80%',
    alignSelf:'center'
  },
  ButtonTouchable1:{
    backgroundColor:'#FF356B',
    borderRadius:30,
    width:45,
    height:45,
    alignItems:'center',
    justifyContent:'center'
  },
  ButtonTouchable2:{
    backgroundColor:'#009D70',
    borderRadius:30,
    width:45,
    height:45,
    alignItems:'center',
    justifyContent:'center'
  }

});
