import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, CheckBox } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from '../constant/Icon';
import ToggleSwitch from 'toggle-switch-react-native';
// import CheckBox from '@react-native-community/checkbox';


export default class configApp extends React.Component {



  render() {

    return (
      <View style={styles.container}>
        <ScrollView style={styles.ScrollView} showsVerticalScrollIndicator={false}>
            <View style={styles.Main}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Icon name='chevron-left' size={28} color='#6979F8'/>
                    </TouchableOpacity>
                    <Text style={{color:'#6979F8',fontSize:16,left:16}}>Voltar</Text>
                </View>
                <View style={{marginTop:14,flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:5}}>
                    <Text style={{fontSize:34}}>Configurações</Text>
                    <TouchableOpacity>
                        <Icon name='setting' type='antdesign' size={22}/>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:49}}>
                    <Text style={{fontSize:17,color:'#6979F8'}}>Lembretes</Text>
                    <View style={{marginTop:19}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                            <Text style={{fontSize:15}}>Lançamento</Text>
                            <ToggleSwitch
                                isOn={true}
                                thumbOnStyle={{backgroundColor:'#6979F8',elevation:2,}}
                                trackOnStyle={{elevation: 1,backgroundColor:'white',borderColor:'black',borderWidth:0.5}}
                                onColor="white"
                                offColor="red"
                                // size={100}
                                // offColor="red"
                                // label="Example label"
                                // labelStyle={{ color: "black", fontWeight: "900" }}
                                size="medium"
                                onToggle={isOn => console.log("changed to : ", isOn)}
                                />
                        </View>
                        <Text style={{fontSize:13,color:'#999999',marginTop:4}}>Receber lembretes de lançamentos do dia</Text>
                    </View>
                    <View style={{marginTop:58}}>
                        <Text style={{fontSize:17,color:'#6979F8'}}>Boletins</Text>
                        <Text style={{marginTop:19,fontWeight:'bold'}}>Receber Boletins por email</Text>
                    </View>
                    <View style={{marginTop:44}}>
                        <Text style={{fontSize:17,color:'#6979F8'}}>Promoções</Text>
                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                            <View>
                                <Text style={{marginTop:19,fontWeight:'bold'}}>Email</Text>
                                <Text style={{marginTop:4,color:'#999999'}}>Receber promoções por email</Text>
                            </View>
                            <CheckBox
                                disabled={false}
                                onFillColor='#6979F8'
                                onCheckColor='#6979F8'
                                color='#6979F8'
                                style={{color:'#6979F8'}}
                                // value={toggleCheckBox}
                                // onValueChange={(newValue) => setToggleCheckBox(newValue)}
                            />
                        </View>
                    </View>
                    <View style={{marginTop:30}}>
                        <Text style={{fontSize:15,fontWeight:'bold'}}>Notificações</Text>
                        <Text style={{marginTop:4,color:'#999999'}}>receber notificações de promoções</Text>
                    </View>
                    <View style={{marginTop:40}}>
                        <Text style={{fontSize:17,color:'#6979F8'}}>Segurança</Text>
                        <View style={{marginTop:19}}>
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                <Text style={{fontSize:15}}>Alertas de Login</Text>
                                <ToggleSwitch
                                    isOn={true}
                                    thumbOnStyle={{backgroundColor:'#6979F8',elevation:2,}}
                                    trackOnStyle={{elevation: 1,backgroundColor:'white',borderColor:'black',borderWidth:0.5}}
                                    onColor="white"
                                    offColor="red"
                                    // size={100}
                                    // offColor="red"
                                    // label="Example label"
                                    // labelStyle={{ color: "black", fontWeight: "900" }}
                                    size="medium"
                                    onToggle={isOn => console.log("changed to : ", isOn)}
                                    />
                            </View>
                            <Text style={{fontSize:13,color:'#999999',marginTop:4}}>Receber alertas de login por email</Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    // justifyContent: 'center',
  },

  ScrollView:{
    width: '100%'
  },
  Main:{
    marginTop:35,
    width:'90%',
    alignSelf:'center',
    marginBottom:20
  },
  

});
