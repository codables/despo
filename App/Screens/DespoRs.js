import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, Dimensions, KeyboardAvoidingView, Linking } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from '../constant/Icon';
import { Avatar, Snackbar } from 'react-native-paper';


export default class DespoRs extends React.Component {



  render() {

    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        
        <View style={styles.HeaderMain}>
            <Image source={require('../assets/images/orange1.png')} style={styles.HeaderImage}/>
            <Text style={styles.HeaderText}>DESPO</Text>
        </View>
        <ScrollView style={styles.ScrollView} showsVerticalScrollIndicator={false}>
            <View style={styles.Main}>
                <View style={styles.ValorMain}>
                    <Text style={styles.ValorText}>Valor</Text>
                    <View style={styles.PriceRsMain}>
                        <Text style={styles.RsText}>R$</Text>
                        <Text style={styles.Price}>0</Text>
                    </View>
                </View>
                <View style={styles.SmallBox1Main}>
                    <View style={styles.ContentMain}>
                        <Text style={styles.EIcon}>E</Text>
                        <Text style={styles.EmpresaText}>Empresa</Text>
                    </View>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoSaldos')}>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.ContentItemsMain}>
                    <View style={styles.ContentMain}>
                        <Avatar.Image size={50} style={styles.AvatarImage}/>
                        <Text style={styles.ContentTextSame}>Selecione o Plano de Conta</Text>
                    </View>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoCad')}>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.ContentItemsMain}>
                    <View style={styles.ContentMain}>
                        <Avatar.Image size={50} style={styles.AvatarImage}/>
                        <Text style={styles.ContentTextSame}>Selecione o Centro de Custo</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.ContentItemsMain}>
                    <View style={styles.ContentMain}>
                        <Icon name='center-align' type='fontisto' color='#CACACA' size={24} style={{left:24}}/>
                        <Text style={styles.ContentTextSame1}>Selecione o Centro de Custo</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.ContentItemsMain}>
                    <View style={styles.ContentMain}>
                        <Icon name='calendar' type='entypo' color='#CACACA' size={28} style={{left:24}}/>
                        <Text style={styles.ContentTextSame1}>Selecione o Centro de Custo</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.ContentItemsMain}>
                    <View style={styles.ContentMain}>
                        <Avatar.Image size={40} style={styles.AvatarImageContaDeSaldo}/>
                        <Text style={styles.ContaDeSaldoText}>Conta de Saldo</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.SmallBox1Main}>
                    <View style={styles.ContentMain}>
                        <Text style={styles.RIcon}>R</Text>
                        <Text style={styles.EmpresaText}>Ricardo Reichert</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='chevron-right' size={24} color='#CACACA'/>
                    </TouchableOpacity>
                </View>
                <View style={styles.BottomIconsMain}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.GreenIcon}>
                        <Icon name='cross' type='entypo' color='white' size={22} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.RedIcon}>
                        <Icon name='check' type='entypo' color='white' size={22} />
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  HeaderMain:{
    backgroundColor:'white',
    width:'100%',
    flexDirection:'row',
    marginTop:20,
    height:60,
    alignItems:'center',
    justifyContent:'center'
  },
  HeaderImage:{
    width:35,
    height:35,
    top:3
  },
  HeaderText:{
    fontSize:16,
    left:5,
    fontWeight:'bold',
    top:3
  },
  ScrollView:{
    width: '100%'
  },
  Main:{
    marginTop:15
  },
  ValorMain:{
    backgroundColor:'white',
    height:100,
    width:'100%'
  },
  ValorText:{
    fontSize:16,
    marginLeft:85,
    top:10
  },
  PriceRsMain:{
    flexDirection:'row',
    marginLeft:20,
    marginTop:10,
    alignItems:'center'
  },
  RsText:{
    backgroundColor:'white',
    elevation:4,
    width:50,
    height:30,
    borderRadius:5,
    textAlign:'center',
    textAlignVertical:'center',
    fontSize:18,
    color:'#606060'
  },
  Price:{
    fontSize:42,
    left:20
  },
  SmallBox1Main:{
    backgroundColor:'white',
    marginTop:2,
    height:60,
    width:'100%',
    justifyContent:'space-between',
    alignItems:'center',
    flexDirection:'row'
  },
  ContentItemsMain:{
    backgroundColor:'white',
    marginTop:2,
    height:80,
    width:'100%',
    justifyContent:'space-between',
    alignItems:'center',
    flexDirection:'row'
  },
  ContentMain:{
    flexDirection:'row',
    alignItems:'center'
  },
  EIcon:{
    backgroundColor:'rgba(61, 82, 185, 0.6)',
    borderRadius:20,
    width:40,
    height:40,
    left:20,
    textAlignVertical:'center',
    textAlign:'center'
  },
  EmpresaText:{
    left:45
  },
  AvatarImage:{
    left:15,
    backgroundColor:'#C4C4C4'
  },
  ContentTextSame:{
    left:35,
    fontSize:18,
    color:'#CACACA'
  },
  ContentTextSame1:{
    left:55,
    fontSize:18,
    color:'#CACACA'
  },
  AvatarImageContaDeSaldo:{
    left:20,
    backgroundColor:'rgba(61, 82, 185, 0.6)'
  },
  ContaDeSaldoText:{
    left:45,
    fontSize:18,
    color:'#CACACA'
  },
  RIcon:{
    backgroundColor:'rgba(255, 172, 48, 0.72)',
    borderRadius:20,
    width:40,
    height:40,
    left:20,
    textAlignVertical:'center',
    textAlign:'center'
  },
  BottomIconsMain:{
    marginTop:40,
    marginBottom:30,
    alignItems:'center',
    justifyContent:'space-between',
    flexDirection:'row',
    width:'80%',
    alignSelf:'center'
  },
  GreenIcon:{
    backgroundColor:'#FF356B',
    borderRadius:30,
    width:45,
    height:45,
    alignItems:'center',
    justifyContent:'center'
  },
  RedIcon:{
    backgroundColor:'#009D70',
    borderRadius:30,
    width:45,
    height:45,
    alignItems:'center',
    justifyContent:'center'
  }

});
