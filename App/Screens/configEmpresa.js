import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, Dimensions, KeyboardAvoidingView, Linking } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from '../constant/Icon';
import { Avatar, Divider, Snackbar } from 'react-native-paper';

const screenWidth = Dimensions.get('window').width;
const headerHeight = Dimensions.get('window').height;
// const windowHeight = Dimensions.get('window').height;


export default class configEmpresa extends React.Component {



  render() {

    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        <View style={{ marginTop: -10,flex:1 }}>
            <View style={styles.Main} />
            <View style={styles.curvedLine}/>
            <View style={{position:'absolute',marginTop:40,height:205,width:'90%',alignSelf:'center'}}>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{}} onPress={()=>this.props.navigation.goBack()}>
                  <Icon name='chevron-left' type='entypo' size={20}/>
                </TouchableOpacity>
                <Image source={require('../assets/images/DespoWhiteIcon.png')} style={{width:43,height:47,left:120}}/>
              </View>
                <View style={{backgroundColor:'#D0D0D0',width:115,height:115,marginTop:36,alignItems:'center',alignSelf:'center',justifyContent:'center',bottom:10,position:'absolute',borderRadius:8,borderWidth:4,borderColor:'#F7DEBA'}}>
                  <View>
                    <Image source={require('../assets/images/DespoWhiteIcon.png')} style={{width:43,height:47}}/>
                  </View>
                  <View style={{position:'absolute',height:115,width:115,alignItems:'flex-end'}}>
                    <TouchableOpacity style={{right:15,top:15}}>
                      <Image source={require('../assets/images/pencilIcon.png')} style={{width:23,height:23}}/>
                      {/* <Icon name='edit-2' type='feather' size={20} style={{}}/> */}
                    </TouchableOpacity>
                  </View>
                </View>
              <View>
              </View>
            </View>
            <ScrollView style={{flex:1}} showsVerticalScrollIndicator={false}>
              <View style={{marginBottom:30,width:'100%',marginTop:20}}>
                  <View style={{backgroundColor:'white',width:'90%',height:142,alignSelf:'center',elevation:3,borderRadius:5}}>
                      <View>
                        <Text style={{left:25,marginTop:12,fontSize:15,fontWeight:'bold'}}>Membros</Text>
                        <Divider style={{width:'88%',alignSelf:'center',padding:1,left:5,marginTop:4}}/>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <View style={{width:100,height:80,marginTop:10}}>
                                <View style={{width:80,height:80,marginLeft:40,justifyContent:'center'}}>
                                    <Image source={require('../assets/images/profileImage6.jpg')} style={{width:60,height:60,borderWidth:2,borderColor:'white',borderRadius:35}} size={65}/>
                                    <View style={{position:'absolute',right:29}}>
                                        <Image source={require('../assets/images/profileImage4.jpeg')} style={{width:60,height:60,borderWidth:2,borderColor:'white',borderRadius:35}} size={65}/>
                                        <View style={{position:'absolute',right:18}}>
                                            <Image source={require('../assets/images/profileImage1.jpg')} style={{width:60,height:60,borderWidth:2,borderColor:'white',borderRadius:35}} size={65}/>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{width:103,marginLeft:6,marginTop:15,height:65,justifyContent:'center'}}>
                                <Text style={{fontSize:10,color:'#4F4F4F'}}>Beatriz, Laura e Angela fazem parte da equipe.</Text>
                            </View>
                            <View style={{width:103,marginTop:15,height:65,justifyContent:'center',marginLeft:10}}>
                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('configApp')} style={{backgroundColor:'#C4C4C4',width:40,height:40,alignItems:'center',justifyContent:'center',borderRadius:30}}>
                                        <Icon name='plus' type='entypo' size={20} style={{fontWeight:'bold'}}/>
                                    </TouchableOpacity>
                                    <Text style={{fontSize:12,color:'#4F4F4F',left:6}}>Adicionar</Text>
                                </View>
                            </View>
                        </View> 
                      </View>
                  </View>

                  <View style={{height:274,backgroundColor:'white',elevation:4,marginTop:20}}>
                      <View style={{height:252,width:'90%',alignSelf:'center',borderRadius:10,backgroundColor:'white',elevation:2,marginTop:10}}>
                          <View style={{marginTop:20}}>
                            <Text style={{fontSize:18,textAlign:'center',fontWeight:'bold'}}>Composição de Saldo</Text>
                                <View style={{marginTop:26}}>
                                    <Divider style={{padding:1}}/>
                                    <View style={{alignSelf:'flex-end',marginTop:-27,right:10,}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('configApp')} style={{backgroundColor:'#FFAC30',width:50,height:50,alignItems:'center',justifyContent:'center',borderRadius:30}}>
                                        <Icon name='plus' type='entypo' size={20} style={{fontWeight:'bold'}}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={{marginLeft:29,flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:25}}>
                                    <View style={{}}>
                                        <Text style={{fontSize:18}}>Caixa</Text>
                                        <Text style={{fontSize:18,color:'#009D70'}}>R$ 905,62</Text>
                                    </View>
                                    <TouchableOpacity>
                                        <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{marginLeft:29,marginTop:31,flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:25}}>
                                    <View>
                                        <Text style={{fontSize:18}}>Banco Geral</Text>
                                        <Text style={{fontSize:18,color:'#009D70'}}>R$ 905,62</Text>
                                    </View>
                                    <TouchableOpacity>
                                        <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                          </View>
                      </View>
                  </View>


                  <View style={{height:444,backgroundColor:'white',elevation:4,marginTop:20}}>
                      <View style={{height:421,width:'90%',alignSelf:'center',borderRadius:10,backgroundColor:'white',elevation:2,marginTop:10}}>
                          <View style={{marginTop:20}}>
                            <Text style={{fontSize:18,textAlign:'center',fontWeight:'bold'}}>Centro de Custo</Text>
                                <View style={{marginTop:26}}>
                                    <Divider style={{padding:1}}/>
                                    <View style={{alignSelf:'flex-end',marginTop:-27,right:10,}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('configApp')} style={{backgroundColor:'#FFAC30',width:50,height:50,alignItems:'center',justifyContent:'center',borderRadius:30}}>
                                        <Icon name='plus' type='entypo' size={20} style={{fontWeight:'bold'}}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={{marginLeft:29,marginRight:25}}>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Geral</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Administrativo</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Produção</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Financeiro</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Recursos Humanos</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Comercial</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'space-between',marginTop:18}}>
                                        <Text style={{fontSize:18,fontWeight:'500'}}>Geral</Text>
                                        <TouchableOpacity>
                                            <Image source={require('../assets/images/Vector3.png')} style={{ width:22,height:22}} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                          </View>
                      </View>
                  </View>
                  
                  <View style={{height:444,backgroundColor:'white',elevation:4,marginTop:20}}>
                      <View style={{height:421,width:'90%',alignSelf:'center',borderRadius:10,backgroundColor:'white',elevation:2,marginTop:10}}>
                          <View style={{marginTop:20}}>
                            <Text style={{fontSize:18,textAlign:'center',fontWeight:'bold'}}>Plano de Contas</Text>
                                <View style={{marginTop:26}}>
                                    <Divider style={{padding:1}}/>
                                <View>
                                    <View style={{marginTop:10,marginLeft:25,width:'80%'}}>
                                        <Text style={{color:'#606060'}}>1.0.0.00. ENTRADAS 1.1.0.00. OPERACIONAL</Text>
                                    </View>
                                    <View style={{height:57,marginTop:2,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingLeft:25,paddingRight:20,backgroundColor:'white',elevation:2,width:'100%'}}>
                                        <Text style={{fontWeight:'bold'}}>1.1.1.01. Venda à Vista</Text>
                                        <TouchableOpacity>
                                            <Icon name='chevron-right' size={24} color='#CACACA'/>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{height:57,marginTop:2,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingLeft:25,paddingRight:20,backgroundColor:'white',elevation:2,width:'100%'}}>
                                        <Text style={{fontWeight:'bold'}}>1.1.1.02. Recebimento da Venda à Prazo</Text>
                                        <TouchableOpacity>
                                            <Icon name='chevron-right' size={24} color='#CACACA'/>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{height:57,marginTop:2,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingLeft:25,paddingRight:20,backgroundColor:'white',elevation:2,width:'100%'}}>
                                        <Text style={{fontWeight:'bold'}}>1.1.1.03. Recebimento de Cheque</Text>
                                        <TouchableOpacity>
                                            <Icon name='chevron-right' size={24} color='#CACACA'/>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{height:57,marginTop:2,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingLeft:25,paddingRight:20,backgroundColor:'white',elevation:2,width:'100%'}}>
                                        <Text style={{fontWeight:'bold'}}>1.1.1.04. Recebimento de Cheque</Text>
                                        <TouchableOpacity>
                                            <Icon name='chevron-right' size={24} color='#CACACA'/>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{height:57,marginTop:2,flexDirection:'row',alignItems:'center',justifyContent:'space-between',paddingLeft:25,paddingRight:20,backgroundColor:'white',elevation:2,width:'100%'}}>
                                        <Text style={{fontWeight:'bold'}}>1.1.1.05.</Text>
                                        <TouchableOpacity>
                                            <Icon name='chevron-right' size={24} color='#CACACA'/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                          </View>
                      </View>
                  </View>
              </View>
            </ScrollView>
        </View>
       
 {/* <Circle
              cx={screenWidth / 2}
              cy={`-${898 - 200 + 2}`}
              r="898.5"
              fill="#EFF2F3"
              stroke="#C5CACD"
              strokeWidth="2"
            /> */}
        {/* <ScrollView>
        </ScrollView> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  Main:{
    width: "100%",
    height: 240,
    borderRadius: 12,
    // backgroundColor: "blue",
  },
  curvedLine: {
    width: "22%",
    height: 220,
    position: "absolute",
    // zIndex:99,
    // bottom: -25,
    top:-20,
    left: "40%",
    borderRadius: 35,
    backgroundColor: "#EEBF7A",
    transform: [{ scaleX: 5 }, { scaleY: 1 }],
  },
  
    // Main: {
    //     alignSelf: 'center',
    //     width: window.width,
    //     overflow: 'hidden',
    //     height: window.width / 1.7
    // },
    // curvedLine: {
    //     borderRadius: window.width,
    //     width: window.width * 2,
    //     height: window.width * 2,
    //     marginLeft: -(window.width / 2),
    //     position: 'absolute',
    //     bottom: 0,
    //     overflow: 'hidden'
    // },
});
