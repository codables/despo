import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView,  Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SimpleButton } from '../constant/Button';
import {  TextInputNative1 } from '../constant/TextInputs';
import { Icon } from '../constant/Icon';
import ToggleSwitch from 'toggle-switch-react-native'
import LinearGradient from 'react-native-linear-gradient';
const screenWidth = Dimensions.get('window').width;
const headerHeight = Dimensions.get('window').height;
// const windowHeight = Dimensions.get('window').height;
export default class DespoCad extends React.Component {

  render() {

    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        
        <View style={{ marginTop: -10,flex:1 }}>
            <View style={styles.Main} />
            <View style={styles.curvedLine}/>
            <View style={{position:'absolute',marginTop:40,height:205,width:'90%',alignSelf:'center'}}>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{}} onPress={()=>this.props.navigation.goBack()}>
                  <Icon name='chevron-left' type='entypo' size={20}/>
                </TouchableOpacity>
                <Image source={require('../assets/images/DespoWhiteIcon.png')} style={{width:43,height:47,left:120}}/>
              </View>
                <View style={{backgroundColor:'#D0D0D0',width:115,height:115,marginTop:36,alignItems:'center',alignSelf:'center',justifyContent:'center',bottom:10,position:'absolute',borderRadius:8,borderWidth:4,borderColor:'#F7DEBA'}}>
                  <View>
                    <Image source={require('../assets/images/DespoWhiteIcon.png')} style={{width:43,height:47}}/>
                  </View>
                  <View style={{position:'absolute',height:115,width:115,alignItems:'flex-end'}}>
                    <TouchableOpacity style={{right:15,top:15}}>
                      <Image source={require('../assets/images/pencilIcon.png')} style={{width:23,height:23}}/>
                      {/* <Icon name='edit-2' type='feather' size={20} style={{}}/> */}
                    </TouchableOpacity>
                  </View>
                </View>
              <View>
              </View>
            </View>
            <ScrollView style={{flex:1}} showsVerticalScrollIndicator={false}>
              <View style={{left:25,marginBottom:30}}>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>CNPJ</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>Razão Social</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>Nome Fantasia</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>CEP</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>Endereço Completo</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10}}>
                  <Text style={{fontSize:15,fontWeight:'500'}}>Atividade Principal</Text>
                  <TextInputNative1/>
                </View>
                <View style={{marginTop:10,flexDirection:'row',width:'85%',alignItems:'center',justifyContent:'space-between'}}>
                  <View style={{width:'50%'}}>
                    <Text style={{fontSize:15,fontWeight:'500'}}>Cidade</Text>
                    <TextInputNative1 textInputStyle={{width:'100%'}}/>
                  </View>
                  <View style={{width:'30%'}}>
                    <Text style={{fontSize:15,fontWeight:'500'}}>Estado</Text>
                    <TextInputNative1 textInputStyle={{width:'100%'}}/>
                  </View>
                </View>
                <View style={{marginTop:90,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                  <View style={{flexDirection:'row',width:'30%',alignItems:'center'}}>
                  <ToggleSwitch
                    isOn={true}
                    thumbOnStyle={{backgroundColor:'#FFAC30',elevation:2,}}
                    trackOnStyle={{elevation: 1,backgroundColor:'white',borderColor:'black',borderWidth:0.5}}
                    onColor="white"
                    offColor="red"
                    // size={100}
                    // offColor="red"
                    // label="Example label"
                    // labelStyle={{ color: "black", fontWeight: "900" }}
                    size="medium"
                    onToggle={isOn => console.log("changed to : ", isOn)}
                  />
                  <Text style={{fontSize:16,left:13,color:'#009D70'}}>Ativa</Text>
                  </View>
                  <View style={{width:'60%'}}>
                    <SimpleButton title='salvar' onPress={()=>this.props.navigation.navigate('configEmpresa')} touchableStyle={{width:'75%',height:43}} style={{backgroundColor:'#FFAC30',height:43}} textStyle={{fontSize:16}}/>
                  </View>
                </View>
              </View>
            </ScrollView>
        </View>
       
 {/* <Circle
              cx={screenWidth / 2}
              cy={`-${898 - 200 + 2}`}
              r="898.5"
              fill="#EFF2F3"
              stroke="#C5CACD"
              strokeWidth="2"
            /> */}
        {/* <ScrollView>
        </ScrollView> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  Main:{
    width: "100%",
    height: 240,
    borderRadius: 12,
    // backgroundColor: "blue",
  },
  curvedLine: {
    width: "22%",
    height: 220,
    position: "absolute",
    // zIndex:99,
    // bottom: -25,
    top:-20,
    left: "40%",
    borderRadius: 35,
    backgroundColor: "#EEBF7A",
    transform: [{ scaleX: 5 }, { scaleY: 1 }],
  },
  
});
