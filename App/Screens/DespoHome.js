import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  Linking,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {theme} from '../constant/theme';
import {Icon} from '../constant/Icon';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import BottomSheet from 'reanimated-bottom-sheet';
import { Avatar, Divider, Snackbar } from 'react-native-paper';
import { createRef } from 'react';

const FirstRoute = () => (
  <View>
    <View style={{width: '100%'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#009D70'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  </View>
);

const SecondRoute = () => (
  <View>
    <View style={{width: '100%'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#009D70'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  </View>
);
const ThirdRoute = () => (
  <View>
    <View style={{width: '100%'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#009D70'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  </View>
);
const FourthRoute = () => (
  <View>
    <View style={{width: '100%'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#009D70'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 2,
            height: 80,
            width: '100%',
          }}>
          <View style={{width: '88%', alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 17, color: '#151522'}}>Descrição</Text>
              <Text style={{fontSize: 17, color: '#FF356B'}}>R$7811.62</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name="circle"
                  type="fontawesome"
                  size={14}
                  color="#009D70"
                />
                <Text style={{fontSize: 13, color: '#151522', left: 5}}>
                  Categoria
                </Text>
              </View>
              <Text style={{fontSize: 11, color: '#999999'}}>
                Abril 20, 2012 1:54 AM
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  </View>
);

export default class DespoHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: '1', title: 'Dia'},
        {key: '2', title: 'Semana'},
        {key: '3', title: 'Mês'},
        {key: '4', title: 'Análise'},
      ],
    };
    this.bottomSheet = createRef();
  }
  renderContent = () => (
    <View style={styles.SheetMain}>
      <Text style={styles.SheetMainHeading}>Funções</Text>
      <View style={styles.TextInputMain}>
        <TextInput
          placeholder="Busque aqui"
          placeholderColor="#ACAFC2"
          style={styles.TextInput}
        />
        <Icon
          name="search"
          type="fontawesome"
          size={20}
          color="#ACAFC2"
          style={styles.TextInputIcon}
        />
      </View>
      <View style={styles.BottomSheetItemsMain}>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="trending-down"
              type="feather"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="trending-up"
              type="materialicons"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="trending-down"
              type="feather"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="trending-down"
              type="feather"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="checkcircle"
              type="antdesign"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon name="movie-open-outline" size={24} color="#FFAC30" />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="checkcircle"
              type="antdesign"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
        <View style={styles.BottomItems}>
          <TouchableOpacity style={styles.BottomItemsTouchable}>
            <Icon
              name="checkcircle"
              type="antdesign"
              size={24}
              color="#FFAC30"
            />
          </TouchableOpacity>
          <Text style={{color: '#3A4276', fontSize: 10, top: 5}}>Despesas</Text>
        </View>
      </View>
    </View>
  );

  _handleIndexChange = (index) => this.setState({index});

  _renderHeader = (props) => (
    <TabBar
      {...props}
      activeColor="#3D639D"
      inactiveColor={theme.TextColors.TextDarkGray}
      indicatorStyle="rgba(255, 120, 0, 0.57)"
      style={{backgroundColor: 'white', fontSize: 8}}
      labelStyle={{fontSize: 11, fontWeight: 'bold'}}
    />
  );

  _renderScene = SceneMap({
    1: FirstRoute,
    2: SecondRoute,
    3: ThirdRoute,
    4: FourthRoute,
  });
  componentDidMount() {
    // this.openPopUp()
  }
  openPopUp() {
    this.props.navigation.navigate('DespoRs')
    // this.bottomSheet.current.snapTo(0);
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        <View style={styles.HeaderMain}>
          <View style={styles.DespoHeaderMain}>
            <Image
              source={require('../assets/images/orange1.png')}
              style={styles.HeaderImage}
            />
            <Text style={styles.HeaderText}>DESPO</Text>
          </View>
          <TouchableOpacity style={styles.Hamburger} onPress={()=>this.props.navigation.openDrawer()} >
            <Icon name="menu" size={24} />
          </TouchableOpacity>
        </View>
        <ScrollView
          style={styles.ScrollView}
          showsVerticalScrollIndicator={false}>
          <View style={styles.Main}>
            <View style={styles.SliderMainView}>
              <View style={styles.ResumoHeadingMain}>
                <Text style={styles.ResumoHeadingText}>Resumo de caixa</Text>
                {/* <DatePickers/> */}
              </View>
              <ScrollView
                style={styles.SliderScrollView}
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoResumo')}>

                  
                <View style={styles.SliderItemsMain}>
                  <View style={styles.SliderMain}>
                    <Text style={styles.SliderDate}>15/12/2020</Text>
                    <Divider style={styles.SliderDivider} />
                    <Text style={styles.SliderHeading}>Saldo Total do Dia</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice1}>R$</Text>
                      <Text style={styles.SliderPrice1}>-226,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>
                      Saldo Total Anterior
                    </Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>1.005,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>Saldo Total Atual</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>779,03</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      backgroundColor: '#FF356B',
                      padding: 4,
                      position: 'absolute',
                      bottom: 0,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                    }}
                  />
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoResumo')}>
                <View style={styles.SliderItemsMain}>
                  <View style={styles.SliderMain}>
                    <Text style={styles.SliderDate}>15/12/2020</Text>
                    <Divider style={styles.SliderDivider} />
                    <Text style={styles.SliderHeading}>Saldo Total do Dia</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice1}>R$</Text>
                      <Text style={styles.SliderPrice1}>-226,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>
                      Saldo Total Anterior
                    </Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>1.005,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>Saldo Total Atual</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>779,03</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      backgroundColor: '#FF356B',
                      padding: 4,
                      position: 'absolute',
                      bottom: 0,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                    }}
                  />
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoResumo')}>
                <View style={styles.SliderItemsMain}>
                  <View style={styles.SliderMain}>
                    <Text style={styles.SliderDate}>15/12/2020</Text>
                    <Divider style={styles.SliderDivider} />
                    <Text style={styles.SliderHeading}>Saldo Total do Dia</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice1}>R$</Text>
                      <Text style={styles.SliderPrice1}>-226,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>
                      Saldo Total Anterior
                    </Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>1.005,02</Text>
                    </View>
                    <Text style={styles.SliderHeading}>Saldo Total Atual</Text>
                    <View style={styles.SliderPriceMain}>
                      <Text style={styles.SliderPrice2}>R$</Text>
                      <Text style={styles.SliderPrice2}>779,03</Text>
                    </View>
                  </View>
                  <View style={styles.BottomSliderLine} />
                </View>
                </TouchableOpacity>
              </ScrollView>
              <View style={[styles.PlusIconMain]}>
                <TouchableOpacity
                  onPress={() => this.openPopUp()}
                  style={styles.PlusIconTouchable}>
                  <Icon name="plus" size={28} style={{fontWeight: 'bold'}} />
                </TouchableOpacity>
              </View>
            </View>
            <TabView
              renderTabBar={this._renderHeader}
              navigationState={this.state}
              renderScene={this._renderScene}
              onIndexChange={this._handleIndexChange}
              style={{width: '100%', marginTop: 15, backgroundColor: '#E5E5E5'}}
              // initialLayout={initialLayout}
            />
          </View>
        </ScrollView>
        <View style={styles.BottomSheetMain}>
          <BottomSheet
            ref={this.bottomSheet}
            renderContent={this.renderContent}
            snapPoints={[400, 0]}	
            enabledContentTapInteraction={false}
            // height={400}
            // closeOnDragDown={true}
            // closeOnPressMask={true}
            // topBarStyle={styles.topBarStyle}
            // backDropStyle={{elevation: 5}}
            // sheetStyle={styles.SheetStyle}
            ></BottomSheet>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  HeaderMain: {
    backgroundColor: 'white',
    width: '100%',
    flexDirection: 'row',
    marginTop: 20,
    height: 60,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  DespoHeaderMain: {
    flexDirection: 'row',
    alignItems: 'center',
    top: 3,
    left: 25,
  },
  HeaderImage: {
    width: 35,
    height: 35,
  },
  HeaderText: {
    fontSize: 16,
    left: 5,
    fontWeight: 'bold',
  },
  Hamburger: {
    top: 3,
    paddingRight: 15,
  },
  ScrollView: {
    width: '100%',
  },
  Main: {
    marginTop: 15,
  },
  SliderMainView: {
    backgroundColor: 'white',
    height: 320,
    width: '100%',
    alignItems: 'center',
  },
  ResumoHeadingMain: {
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  ResumoHeadingText: {
    fontSize: 20,
    color: '#404040',
  },
  SliderScrollView: {
    width: '100%',
  },
  SliderItemsMain: {
    backgroundColor: 'white',
    elevation: 4,
    width: 250,
    height: 250,
    margin: 10,
    borderRadius: 10,
  },
  SliderMain: {
    width: '100%',
    marginTop: 10,
  },
  SliderDate: {
    fontSize: 20,
    color: '#404040',
    textAlign: 'center',
  },
  SliderDivider: {
    padding: 0.5,
    width: '90%',
    alignSelf: 'center',
  },
  SliderHeading: {
    fontSize: 20,
    left: 15,
    top: 5,
  },
  SliderPriceMain: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  SliderPrice1: {
    fontSize: 22,
    color: '#FF356B',
  },
  SliderPrice2: {
    fontSize: 22,
    color: '#009D70',
  },
  BottomSliderLine: {
    width: '100%',
    backgroundColor: '#FF356B',
    padding: 4,
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  PlusIconMain: {
    position: 'absolute',
    justifyContent: 'center',
    height: 320,
    // width: '90%',
    width:60,
    alignSelf:'flex-end', 
    paddingRight:30,
    alignItems: 'flex-end',
  },
  PlusIconTouchable: {
    backgroundColor: '#FFAC30',
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 35,
  },
  BottomSheetMain: {
    width: '100%',
    marginBottom: 40,
    position: 'absolute',
  },
  SheetStyle: {
    borderRadius: 50,
    width: '100%',
    height: 450,
  },
  SheetMain: {
    marginTop: 20,
    width: '85%',
    alignSelf: 'center',
  },
  SheetMainHeading: {
    fontSize: 16,
    color: '#3A4276',
    fontWeight: 'bold',
  },
  TextInputMain: {
    marginTop: 10,
  },
  TextInput: {
    borderWidth: 1,
    borderColor: 'rgba(31, 32, 65, 0.25)',
    height: 44,
    paddingLeft: 40,
    borderRadius: 5,
  },
  TextInputIcon: {
    position: 'absolute',
    top: 12,
    paddingLeft: 10,
  },
  BottomSheetItemsMain: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  BottomItems: {
    width: 60,
    height: 100,
    alignItems: 'center',
    margin: 5,
  },
  BottomItemsTouchable: {
    backgroundColor: '#F1F3F6',
    elevation: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 5,
  },
});
