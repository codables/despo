import React, {useState, Component} from 'react';
// import React, {useEffect} from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Dimensions,
  Image,
  Alert,
} from 'react-native';
import {Form} from 'native-base';
import {SvgXml} from 'react-native-svg';
let {width, height} = Dimensions.get('screen');
console.log(width, height / width);
// import testSvg from '../assets/images/Group 160.svg';
const testSvg = `<svg width="${'138'}" height="808" viewBox="0 0 138 808" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="2" y="544" width="135" height="135" fill="white"/>
<path d="M68 544C86.0287 544 103.995 549.531 116.744 559.377C129.492 569.223 136.654 582.576 136.654 596.5C136.654 610.424 129.492 623.777 116.744 633.623C103.995 643.469 86.0287 649 68 649L68 596.5V544Z" fill="#FF356B"/>
<ellipse cx="77" cy="596" rx="26" ry="30" fill="white"/>
<rect x="29" y="544" width="40" height="135" fill="#FFAC30"/>
<path d="M57.5 597C57.5 613.569 89.3594 627 75 627C60.6406 627 49 613.569 49 597C49 580.431 60.6406 566 75 566C89.3594 566 55.2952 577 57.5 597Z" fill="white"/>
<rect x="2" y="544" width="27" height="136" fill="#3A4276"/>
<rect x="2" y="672" width="135" height="135" fill="#FF356B"/>
<rect x="2" y="672" width="67.6667" height="67.6667" fill="#FFAC30"/>
<rect x="2" y="739.667" width="67.6667" height="67.6667" fill="#FFAC30"/>
<rect x="69.5" y="739.5" width="67.5" height="67.5" fill="#3A4276"/>
<path d="M70.8334 792C99.6556 792 122.333 768.11 122.333 739.5C122.333 710.89 99.6556 687 70.8334 687C42.0111 687 19.3334 710.89 19.3334 739.5C19.3334 768.11 42.0111 792 70.8334 792Z" fill="#58D8F4" stroke="white" stroke-width="30"/>
<rect x="136.664" y="131" width="135.998" height="136" transform="rotate(-180 136.664 131)" fill="#FFAC30"/>
<rect x="137" y="62.6666" width="68.1667" height="67.6667" transform="rotate(-180 137 62.6666)" fill="#FFAC30"/>
<path d="M67.8595 11.4C39.4835 11.4 16.2913 34.2639 16.2913 62.7C16.2913 91.1361 39.4835 114 67.8595 114C96.2355 114 119.428 91.1361 119.428 62.7C119.428 34.2639 96.2355 11.4 67.8595 11.4Z" fill="#FF356B" stroke="white" stroke-width="30"/>
<path d="M67.1543 131H0.666611L0.666611 -6H67.1543L67.1543 131Z" fill="#FFAC30"/>
<rect y="131" width="135.824" height="137" fill="white"/>
<path d="M74.0815 189H97.4223V214H74.0815V189Z" fill="#58D8F4"/>
<path d="M64.6793 252.751L1.00733 209.672L64.1947 166.921L64.437 209.836L64.6793 252.751Z" fill="#FFAC30"/>
<path d="M1.01477 268L1.01477 197L71.7271 233.92L137 268H1.01477Z" fill="#3A4276"/>
<path d="M1.01477 131H137L66.2877 172.08L1.01477 210V131Z" fill="#FF356B"/>
<path d="M102.496 250L67.9926 232H137V268L102.496 250Z" fill="#58D8F4"/>
<path d="M103.511 150.5L137 131V170L70.0222 170L103.511 150.5Z" fill="#58D8F4"/>
<rect x="1.25586" y="268" width="135" height="138" fill="white"/>
<rect x="1.25586" y="406" width="135" height="138" fill="white"/>
<path d="M33.2559 544H136.256L136.081 465L84.7559 504.567L33.2559 544Z" fill="#FFAC30"/>
<path d="M1.25586 388L1.25586 268L136.256 268.203L68.6409 328L1.25586 388Z" fill="#FF356B"/>
<path d="M1 388.298L38.9638 354.711L136.696 465.176L68.8478 426.737L1 388.298Z" fill="#3A4276"/>
<path d="M136.256 315.885V268H90.8025L113.529 291.942L136.256 315.885Z" fill="#3A4276"/>
<path d="M85.2559 504V465H136.256L110.756 484.5L85.2559 504Z" fill="#FF356B"/>
<path d="M85.2559 436V465H136.256L110.756 450.5L85.2559 436Z" fill="#FF356B"/>
<path d="M57.2559 544H1.25586V464L29.2559 504L57.2559 544Z" fill="#58D8F4"/>
</svg>
`;

export default class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flexDirection: 'row', flex: 1}}>
          {/* <Image style={{height:'100%',width:'30%'}} source={require('../assets/images/home.jpg')} /> */}

          <SvgXml style={{height: 200}} xml={testSvg} />
          <View style={{marginTop: 20, flex: 1}}>
            <Image
              style={{marginTop: 10, marginLeft: 15}}
              source={require('../assets/images/orange.png')}
            />

            <Image
              style={{marginTop: 10, marginLeft: 15}}
              source={require('../assets/images/DESPO.png')}
            />

            <View>
              <View
                style={{
                  justifyContent: 'center',
                  marginLeft: 10,
                  marginTop: '20%',
                }}>
                <Text style={{fontSize: 22, color: 'black'}}>
                  Crie a sua conta aqui
                </Text>
                <Text style={{color: '#A9A9A9'}}>Faça login para usar</Text>
                <Text style={{color: '#A9A9A9'}}>nossos serviços</Text>
              </View>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  marginLeft: 10,
                  marginTop: '10%',
                }}>
                Email
              </Text>
              <View
                style={{
                  height: 45,
                  width: '90%',
                  borderRadius: 6,
                  marginLeft: 10,
                  borderWidth: 1,
                  borderColor: 'black',
                }}>
                <TextInput
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: 15,
                  }}
                  placeholder="exemplo@exemplo.com"
                />
              </View>

              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  marginLeft: 10,
                  marginTop: '5%',
                }}>
                Senha
              </Text>
              <View
                style={{
                  height: 45,
                  width: '90%',
                  borderRadius: 6,
                  marginLeft: 10,
                  borderWidth: 1,
                  borderColor: 'black',
                }}>
                <TextInput
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: 15,
                  }}
                  placeholder="Email"
                />
              </View>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Drawer')}>
                <View
                  style={{
                    height: 50,
                    width: '90%',
                    borderRadius: 6,
                    marginTop: 20,
                    marginLeft: 10,
                    backgroundColor: '#f57c00',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      justifyContent: 'center',
                      fontSize: 17,
                    }}>
                    Entrar
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Recovery')}>
                <Text
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                  }}>
                  Esqueceu sua senha?
                </Text>
                <Text
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginTop: 5,
                  }}>
                  Recuperar
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('cadastro')}>
                <View
                  style={{
                    height: 50,
                    width: '90%',
                    borderRadius: 6,
                    marginTop: 20,
                    marginLeft: 10,
                    backgroundColor: '#f57c00',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      justifyContent: 'center',
                      fontSize: 17,
                    }}>
                    Registre-se
                  </Text>
                </View>
              </TouchableOpacity>

              <Text
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  marginTop: '15%',
                }}>
                ou entre com
              </Text>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <SvgXml
                  style={{
                    justifyContent: 'center',
                    alignSelf: 'center',
                    marginLeft: 25,
                  }}
                  xml={`<svg width="49" height="47" viewBox="0 0 49 47" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M1.48352e-05 5C1.48352e-05 2.23858 2.23859 1.23746e-06 5.00001 1.23746e-06H44C46.7614 1.23746e-06 49 2.23858 49 5V42C49 44.7614 46.7614 47 44 47H5.00002C2.23859 47 1.48352e-05 44.7614 1.48352e-05 42V5Z" fill="#0084F4"/>
<path d="M29.0208 14H26.0208C24.6947 14 23.4229 14.5268 22.4852 15.4645C21.5475 16.4021 21.0208 17.6739 21.0208 19V22H18.0208V26H21.0208V34H25.0208V26H28.0208L29.0208 22H25.0208V19C25.0208 18.7348 25.1261 18.4804 25.3136 18.2929C25.5012 18.1054 25.7555 18 26.0208 18H29.0208V14Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
</svg>`}
                />
                <Image
                  style={{height: 47, width: 49, marginRight: 25}}
                  source={require('../assets/images/google.png')}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
