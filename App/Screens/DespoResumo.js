import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, Dimensions, KeyboardAvoidingView, Linking } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from '../constant/Icon';
import { Avatar, Divider, Snackbar } from 'react-native-paper';


export default class DespoResumo extends React.Component {



  render() {

    return (
      <View style={styles.container}>
        {/* <StatusBar style='dark' backgroundColor='white' /> */}
        <View style={styles.HeaderMain}>
            <View style={styles.ArrowLeftMain}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Icon name='chevron-left' size={28}/>
                </TouchableOpacity>
            </View>
            <View style={styles.DespoHeaderMain}>
                <Image source={require('../assets/images/orange1.png')} style={styles.HeaderImage}/>
                <Text style={styles.HeaderText}>DESPO</Text>
            </View>
        </View>
        <ScrollView style={styles.ScrollView} showsVerticalScrollIndicator={false}>
            <View style={styles.Main}>
                <View style={styles.ResumoDeCaixaMainView}>
                    <View style={styles.ResumoItemsMain}>
                        <Text style={styles.ResumoDeCaixaHeadingText}>Resumo de caixa</Text>
                        {/* <DatePickers/> */}
                        <View style={styles.CalendarMain}>
                            <Text style={styles.CalendarText}>15/12/2020</Text>
                            <Icon name='calendar' type='entypo' size={22} color='#999999' style={{left:10}}/>
                        </View>
                    </View>
                  <View style={styles.ResumoItemsMainView}>
                      <View  style={styles.ResumoItemsMain}>
                          <Text style={styles.ResumoItemsText}>Saldo do Dia</Text>
                          <Text style={styles.ResumoItemsRs}>-226,02</Text>
                      </View>
                      <View  style={styles.ResumoItemsMain}>
                          <Text style={styles.ResumoItemsText}>Saldo Anterior</Text>
                          <Text style={styles.ResumoItemsRs1}>1.005,02</Text>
                      </View>
                      <View  style={styles.ResumoItemsMain}>
                          <Text style={styles.ResumoItemsText}>Saldo Anterior</Text>
                          <Text style={styles.ResumoItemsRs1}>779,03</Text>
                      </View>
                      <View  style={styles.ResumoItemsMain}>
                          <Text style={styles.ResumoItemsText}>Diferença de Caixa</Text>
                          <Text style={styles.ResumoItemsRs}>-723,00</Text>
                      </View>
                      <Text style={styles.VocaText}>Você está com uma diferença de 723,00 em caixa, verifique seus lançamentos de <Text style={{fontWeight:'bold'}}>Saída.</Text></Text>
                      <View style={styles.ResumoBottomLine}/>
                  </View>
                  <View style={styles.RealizerButtonMain}>
                    <TouchableOpacity style={{}} onPress={()=>{this.props.navigation.navigate('DespoSaldos')}}>
                        <Text style={styles.RealizerButtonText}>Realizar Fechamento</Text>
                    </TouchableOpacity>
                  </View> 
                </View>
                <View style={styles.ResumoDeLancMain}>
                    <View style={styles.ResumoDeLancMainHeading}>
                        <Text style={styles.ResumoDeLancHeadingText}>Resumo de Lançamentos</Text>
                    </View>
                  <View style={styles.ResumoDeLancItemsMain}>
                      <View  style={styles.ResumoDeLancItemsMainHeading}>
                          <Text style={styles.ItemsHeading}>TOTAL DE ENTRADAS</Text>
                          <Text style={styles.ResumoDeLancMainHeadingPrice}>R$ 2.911,59</Text>
                      </View>
                      <Divider style={styles.Divider}/>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Venda a Vista</Text>
                          <Text style={styles.ResumoDeLancItemsSubHeadingPrice}>R$ 905,62</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#009D70'/>
                                <Text style={styles.CircleIconText}>Recebimentos</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Recebimentos de Boletos</Text>
                          <Text style={styles.ResumoDeLancItemsSubHeadingPrice}>R$ 2005,89</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#009D70'/>
                                <Text style={styles.CircleIconText}>Recebementos</Text>
                           </View>
                      </View>
                  </View>
                  <View style={{backgroundColor:'white',width:'95%',borderRadius:10,marginTop:10,paddingBottom:10,marginBottom:10,elevation:4}}>
                      <View  style={styles.ResumoDeLancItemsMainHeading}>
                          <Text style={styles.ItemsHeading}>TOTAL DE SAIDAS</Text>
                          <Text style={styles.TotalDeSaidasPrice}>R$ -709.6,50</Text>
                      </View>
                      <Divider style={styles.Divider}/>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>ISS</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -55,62</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>impostos</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Simples Nacional</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -1023,00</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>impostos</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Taxa de Cartão de crédito</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -23,00</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>Despesas com Vendas</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Comissões</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -723,00</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>Despesas com Vendas</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Fornecedor a Vista</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -5057,88</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>Compra de Mercadoria</Text>
                           </View>
                      </View>
                      <View style={{width:'90%',alignSelf:'center',marginTop:14}}>
                          <Text style={styles.ItemsHeading}>Salários</Text>
                          <Text style={styles.TotalDeSaidasSubHeadingPrice}>R$ -1024,00</Text>
                          <View style={styles.CircleIconMain}>
                                <Icon name='circle' type='fontawesome' size={12} color='#FF356B'/>
                                <Text style={styles.CircleIconText}>Despesa de Pessoal</Text>
                           </View>
                      </View>
                  </View>
                  
                </View>
              
                
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  HeaderMain:{
    backgroundColor:'white',
    width:'100%',
    flexDirection:'row',
    marginTop:20,
    height:60,
    alignItems:'center'
  },
  ArrowLeftMain:{
    width:'40%',
    top:3,
    left:5
  },
  DespoHeaderMain:{
    flexDirection:'row',
    width:'60%',
    alignItems:'center'
  },
  HeaderImage:{
    width:35,
    height:35,
    top:3
  },
  HeaderText:{
    fontSize:16,
    left:5,
    fontWeight:'bold',
    top:3
  },
  ScrollView:{
    width: '100%'
  },
  Main:{
    marginTop:15
  },
  ResumoDeCaixaMainView:{
    backgroundColor:'white',
    height:340,
    width:'100%',
    alignItems:'center'
  },
  ResumoItemsMainView:{
    backgroundColor:'white',
    width:'95%',
    borderRadius:10,
    marginTop:10,
    height:220,
    elevation:4
  },
  ResumoItemsMain:{
    width:'90%',
    alignSelf:'center',
    justifyContent:'space-between',
    marginTop:10,
    flexDirection:'row'
  },
  ResumoDeCaixaHeadingText:{
    fontSize:20,
    color:'#404040'
  },
  CalendarMain:{
    width:'45%',
    flexDirection:'row',
    padding:5,
    borderRadius:5,
    elevation:1
  },
  CalendarText:{
    fontSize:18,
    color:'#999999',
    left:2
  },
  ResumoItemsText:{
    fontSize:18,
    color:'#404040'
  },
  ResumoItemsRs:{
    fontSize:18,
    color:'#FF356B'
  },
  ResumoItemsRs1:{
    fontSize:18,
    color:'#009D70'
  },
  VocaText:{
    textAlign:'center',
    textAlignVertical:'center',
    marginTop:10,
    fontSize:16
  },
  ResumoBottomLine:{
    width:'100%',
    backgroundColor:'#FF356B',
    padding:4,
    position:'absolute',
    bottom:0,
    borderBottomLeftRadius:10,
    borderBottomRightRadius:10
  },
  RealizerButtonMain:{
    borderWidth:1,
    borderColor:'#FF7800',
    width:'90%',
    height:45,
    marginTop:10,
    alignItems:'center',
    justifyContent:'center'
  },
  RealizerButtonText:{
    fontSize:18,
    color:'#FF7800'
  },
  ResumoDeLancMain:{
    backgroundColor:'white',
    flex:1,
    width:'100%',
    alignItems:'center',
    marginTop:15
  },
  ResumoDeLancMainHeading:{
    width:'90%',
    alignSelf:'center',
    justifyContent:'space-between',
    marginTop:15
  },
  ResumoDeLancHeadingText:{
    fontSize:20,
    color:'#404040'
  },
  ResumoDeLancItemsMain:{
    backgroundColor:'white',
    width:'95%',
    borderRadius:10,
    marginTop:10,
    elevation:4,
    paddingBottom:10
  },
  ResumoDeLancItemsMainHeading:{
    width:'90%',
    alignSelf:'center',
    justifyContent:'space-between',
    marginTop:14,
    flexDirection:'row'
  },
  ItemsHeading:{
    fontSize:18,
    color:'#404040',
    fontWeight:'500'
  },
  ResumoDeLancMainHeadingPrice:{
    fontSize:18,
    color:'#009D70'
  },
  TotalDeSaidasPrice:{
    fontSize:18,
    color:'#FF356B'
  },
  Divider:{
    fontSize:18,
    color:'#FF356B'
  },
  ResumoDeLancItemsSubHeadingPrice:{
    fontSize:17,
    color:'#009D70',
    textAlign:'right'
  },
  CircleIconMain:{
    flexDirection:'row',
    alignItems:'center'
  },
  CircleIconText:{
    fontSize:13,
    color: '#151522',
    left:5
  },
  TotalDeSaidasSubHeadingPrice:{
    fontSize:17,
    color:'#FF356B',
    textAlign:'right'
  }

});
