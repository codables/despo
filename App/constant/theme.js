import { DarkTheme, DefaultTheme} from 'react-native-paper';


export const theme = {
    ...DefaultTheme,
    
    mode: 'adaptive',
    colors: {
        ...DefaultTheme.colors,
        primary: 'white',
        accent: '#C9EF9F',
          
    },

    ScreenColors:{
        NavyBlue:'#31435B',
        LightGray:'#FAFAFA',
        sky:'#4FC3C5',
        Mediumsky:'#19D7D3',
        Lightsky:'#D1F7F6',
        tabColor:'#D1D1D1'
       

    },
    TextColors:{
        NavyBlue:'#31435B',
        twitter:'#00B9FF',
        facebook:'#4267B2',
        sky:'#4FC3C5',
        Mediumsky:'#19D7D3',
        TextLightGray:'#A9A9A9',
        TextLightoffwhite:'#D7DAE1',
        TextDarkBlueish:'#5A697C',
        TextDarkGray:'#9B9AA7',
        ProgressYellow:'#FFA417'
    },

    statusBarColor: 'gray',
    padding:{
        sml:{
            padding:10
        },
        md:{
            padding:15
        },
        lg:{
            padding:20
        },
    },
    fontSize:{
        xxxsm:{
            fontSize:10
        },
        xxsm:{
            fontSize:12
        },
          xsm:{
            fontSize:14
        },
        sm:{
            fontSize:15
        },
        md:{
            fontSize:17
        },
        mdbg:{
            fontSize:20
        },
        bg:{
            fontSize:24
        },
      
        lg:{
            fontSize:30
        },
        xlg:{
            fontSize:50
        },
        xl:{
            fontSize:70
        }
    },
    fontfamily:{
        reg:{
            fontFamily:'Mukta-Regular'
        },
        bold:{
            fontFamily:'Mukta-Bold'
        },
        med:{
            fontFamily:'Mukta-Medium'
        },
        lght:{
            fontFamily:'Mukta-Light'
        },
        merewether:{
            reg:{
            fontFamily:'Merriweather-Regular'
        },
        bold:{
            fontFamily:'Merriweather-Bold'
        },
        med:{
            fontFamily:'Merriweather-Medium'
        },
        lght:{
            fontFamily:'Merriweather-Light'
        }
        }
    },
    p:{
        sml:{
            fontSize:10
        },
        md:{
            fontSize:15
        },
        lg:{
            fontSize:25
        }
    },
    margin:{
        sml:{
            marginTop:18
        },
        md:{
            marginTop:20
        },
        lg:{
            marginTop:35
        },
        xl:{
            marginTop:45
        },
        xlg:{
            marginTop:60
        }
        
    },
    gradientColors: ['#FF8D4E', '#FF7B69']
};
