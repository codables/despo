import Feather from  'react-native-vector-icons/Feather'
import AntDesign from  'react-native-vector-icons/AntDesign'
import FontAwesome5 from  'react-native-vector-icons/FontAwesome5'
import FontAwesome from  'react-native-vector-icons/FontAwesome'
import Entypo from  'react-native-vector-icons/Entypo'
import Fontisto from  'react-native-vector-icons/Fontisto'
import MaterialCommunityIcons from  'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from  'react-native-vector-icons/MaterialIcons'
import Foundation from  'react-native-vector-icons/Foundation'

export { Feather, AntDesign, FontAwesome5, FontAwesome, Entypo,Fontisto, MaterialCommunityIcons, MaterialIcons,Foundation} ;

