import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { Icon } from './Icon';





export const TextInputNative = ({ textInputStyle, value ,title,onChange,onChangee,secureTextEntry,editable,placeholderColor,textContentType,dataDetectorTypes,keyboardType }) => {
    return (
        
       
        <TextInput
        placeholder={title}
        placeholderTextColor={placeholderColor}
        textContentType={textContentType}
        dataDetectorTypes={dataDetectorTypes}
        keyboardType={keyboardType}
        value={value}
        editable={editable}
        style={[styles.TextInputStyle,textInputStyle]}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangee}
        onChange={onChange}
        />
       
    );
}
export const TextInputNative1 = ({ textInputStyle, value ,title,onChange,onChangee,secureTextEntry,editable,placeholderColor,textContentType,dataDetectorTypes,keyboardType }) => {
    return (
        
       
        <TextInput
        placeholder={title}
        placeholderTextColor={placeholderColor}
        textContentType={textContentType}
        dataDetectorTypes={dataDetectorTypes}
        keyboardType={keyboardType}
        value={value}
        editable={editable}
        style={[styles.TextInputStyle1,textInputStyle]}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangee}
        onChange={onChange}
        />
       
    );
}
// export const TextInputIcon = ({ textInputStyle, value ,title,onChange,onChangee,secureTextEntry,editable,placeholderColor,textContentType,dataDetectorTypes,keyboardType }) => {
//     return (
        
//        <View>

//         <TextInput
//         placeholder={title}
//         placeholderTextColor={placeholderColor}
//         textContentType={textContentType}
//         dataDetectorTypes={dataDetectorTypes}
//         keyboardType={keyboardType}
//         value={value}
//         editable={editable}
//         style={[styles.TextInputStyle,textInputStyle]}
//         secureTextEntry={secureTextEntry}
//         onChangeText={onChangee}
//         onChange={onChange}
//         />
//         <View style={{position:'absolute',top:20,right:20,zIndex:99}}>
//         <Icon name='eye' />
//         </View>
//         </View>
       
//     );
// }

const styles = StyleSheet.create({
    TextInputStyle:{
        backgroundColor:'#ffffff',
        width:'85%',
        padding:7,
        paddingLeft:20,
        borderRadius:5,
        elevation:1,
        margin:5,
    },
    TextInputStyle1:{
        // backgroundColor:'#ffffff',
        width:'85%',
        height:43,
        borderColor:'rgba(228, 228, 228, 0.6)',
        // padding:7,
        paddingLeft:20,
        borderRadius:5,
        elevation:1,
        marginTop:10,
    }
  });