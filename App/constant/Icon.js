import React from 'react';
import { Feather, AntDesign, FontAwesome5, FontAwesome, Entypo,Fontisto, MaterialCommunityIcons, MaterialIcons,Foundation} from './IconSet';



export const Icon = ({ size, style = null, name, type = null, color }) => {

    switch (type) {
        case 'entypo':
            return <Entypo style={style} name={name} size={size} color={color} />
        
        case 'feather':
            return <Feather style={style} name={name} size={size} color={color} />
        case 'antdesign':
            return <AntDesign style={style} name={name} size={size} color={color} />
        case 'fontawesome5':
            return <FontAwesome5 style={style} name={name} size={size} color={color} />
        case 'fontawesome':
            return <FontAwesome style={style} name={name} size={size} color={color} />
        case 'fontisto':
            return <Fontisto style={style} name={name} size={size} color={color} />
        case 'materialicons':
            return <MaterialIcons style={style} name={name} size={size} color={color} />
        case 'foundation':
            return <Foundation style={style} name={name} size={size} color={color} />
    }
    return (

        <MaterialCommunityIcons style={style} name={name} size={size} color={color}
        />

    );
}