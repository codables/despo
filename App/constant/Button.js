import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from "../constant/Icon";
import { theme } from './theme';






export const SimpleButton = ({ style = null,  textStyle,touchableStyle, title,onPress ,type = null,  }) => {
    return (
        
       
        <TouchableOpacity onPress={onPress} style={touchableStyle}>
            <View style={[{ backgroundColor: theme.ScreenColors.NavyBlue, width: '100%', borderRadius: 5, alignItems: 'center' }, style]}>
                <Text style={[{ padding: 10, color: 'white' }, textStyle]}>{title}</Text>
            </View>
        </TouchableOpacity>
       
    );
}

export const IconButton = ({ style = null,  textStyle,touchableStyle, title,onPress ,type = null,  }) => {
    return (
        
       
        <TouchableOpacity onPress={onPress} style={touchableStyle}>
            <View style={[{ backgroundColor: '#4FC0FC', width: '100%', borderRadius: 20, alignItems: 'center',justifyContent:'center',flexDirection:'row' }, style]}>
                <Icon name='pencil' type='foundation' size={20} color='white'/>
                <Text style={[{ padding: 10, color: 'white',fontSize:17,fontWeight:'bold' }, textStyle]}>{title}</Text>
            </View>
        </TouchableOpacity>
       
    );
}