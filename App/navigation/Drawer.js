import * as React from 'react';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Dimensions,
  Image,
  Alert,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {DrawerScreens} from './Stacks';
import Animated from 'react-native-reanimated';
const Drawer = createDrawerNavigator();

const DrawerContent = (props) => {
//   console.log('props', props.progress);
  return (
    <DrawerContentScrollView
      {...props}
      contentContainerStyle={{
        backgroundColor: '#F1F3F6',
        // backgroundColor: 'blue',
        flex: 1,
      }}>
      <StatusBar hidden />
      <View
        style={{
          flexDirection: 'row',
          //   flex: 1,
          marginTop: -5,
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            borderBottomRightRadius: 53,
            backgroundColor: '#FFFFFF',
            height: 107,
            // justifyContent: 'center',
            alignItems: 'center',
            //   position: 'absolute',
            flexDirection: 'row',
            paddingLeft: 20,
            paddingRight: 31,
            // flex: 1,
          }}>
          <View
            style={{
              height: 45,
              width: 45,
              borderRadius: 45,
              borderWidth: 2,
              overflow: 'hidden',
              backgroundColor: '#fff',
              borderColor: 'rgba(255, 255, 255, 0.6)',
            }}>
            <Image
              resizeMode="center"
              style={{
                width: '100%',
                height: '100%',
              }}
              source={require('../assets/images/avator.png')}
            />
          </View>
          <View style={{}}>
            <Text style={{fontSize: 16, fontWeight: '500'}}>Fulana de Tal</Text>
            <Text style={{fontSize: 10, fontWeight: '500'}}>Função</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'column',
          flex: 1,
          paddingLeft: 10,
        }}>
        <View style={{flex: 1, paddingTop: 80}}>
          <DrawerItem
            label="Inicio"
            labelStyle={{
              color: 'black',
              fontSize: 16,
              fontWeight: '700',
            }}
            onPress={() => {props.navigation.closeDrawer(); props.navigation.navigate('HomeStack')}}
          />
          <DrawerItem
            label="Perfil"
            labelStyle={{
              color: 'black',
              fontSize: 16,

              fontWeight: '500',
            }}
            onPress={() =>  {props.navigation.closeDrawer();props.navigation.navigate('Profile')}}
          />
          <DrawerItem
            label="Empresas"
            labelStyle={{
              color: 'black',
              fontSize: 16,

              fontWeight: '500',
            }}
            onPress={() => {props.navigation.closeDrawer(); props.navigation.navigate('EmpresaStack')}}
          />
          <DrawerItem
            label="Configurações"
            labelStyle={{
              color: 'black',
              fontSize: 16,

              fontWeight: '500',
            }}
            onPress={() =>  {props.navigation.closeDrawer();props.navigation.navigate('configApp')}}
          />
          <DrawerItem
            label="Ajuda"
            labelStyle={{
              color: 'black',
              fontSize: 16,
              fontWeight: '500',
            }}
          />
          {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate('DespoHome')}><Text>Inicio</Text></TouchableOpacity>
           <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}><Text style={{marginTop: '10%'}} >Perfil</Text></TouchableOpacity>
           <TouchableOpacity onPress={()=>this.props.navigation.navigate('Empresa')}><Text style={{marginTop: '10%'}}>Empresas</Text></TouchableOpacity>
           <TouchableOpacity onPress={()=>this.props.navigation.navigate('configApp')}><Text style={{marginTop: '10%'}}>Configurações</Text></TouchableOpacity>
           <TouchableOpacity ><Text style={{marginTop: '10%'}}>Ajuda</Text></TouchableOpacity> */}
        </View>
        <View
          style={{
            paddingLeft: 15,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Login')}
            style={{
              flexDirection: 'row',
              marginBottom: 40,
            }}>
            <Icon
              name="power"
              style={{fontWeight: '700'}}
              size={18}
              color="#1B1D28"
            />
            <Text style={{marginLeft: 5, fontWeight: '700', color: '#1B1D28'}}>
              Sair
            </Text>
          </TouchableOpacity>
          <Text
            style={{
              fontWeight: '500',
              marginLeft: 1,
              fontSize: 10,
              marginBottom: 27,
              color: '#3A4276',
            }}>
            Version 0.0.1
          </Text>
        </View>
      </View>
    </DrawerContentScrollView>
  );
};

export default () => {
  const [progress, setProgress] = React.useState(new Animated.Value(0));
  const scale = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.67],
  });
  const rotateZ = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: ['0deg', '-5deg'],
  });
  const borderRadius = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [0, 30],
  });
  const screensStyle = {zIndex:2, borderRadius, transform: [{scale}, {rotateZ}]};
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity style={{position:'absolute', right: 28, top:40,}}>
          <Icon name="close" size={30} color="#900" />
      </TouchableOpacity>

      <Drawer.Navigator
        overlayColor="transparent"
        drawerType="slide"
        drawerContentOptions={
          {
            //   activeBackgroundColor:'red',
            //   activeTintColor:'blue',
            //   inactiveBackgroundColor:'yellow',
            //   inactiveTintColor:'green'
          }
        }
        drawerStyle={{width: '56%'}}
        sceneContainerStyle={{
          backgroundColor: 'transparent',
        }}
        // openByDefault
        initialRouteName="Screens"
        drawerContent={(props) => {
          setProgress(props.progress);
          return <DrawerContent {...props} />;
        }}>
        <Drawer.Screen name="Screens">
          {(props) => <DrawerScreens {...props} style={screensStyle} />}
        </Drawer.Screen>
      </Drawer.Navigator>
    </View>
  );
};
