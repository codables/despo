import * as React from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import cadastroScreen from '../Screens/cadastro';
import LoginScreen from '../Screens/Login';
import RecoveryScreen from '../Screens/Recovery';
import MenuScreen from '../Screens/Menu';
import ProfileScreen from '../Screens/Profile';
import EmpresaScreen from '../Screens/Empresa';

import DespoCad from '../Screens/DespoCad';
import configEmpresa from '../Screens/configEmpresa';
import configApp from '../Screens/configApp';
import DespoRs from '../Screens/DespoRs';
import DespoSaldos from '../Screens/DespoSaldos';
import DespoResumo from '../Screens/DespoResumo';
import DespoHome from '../Screens/DespoHome';
import Animated from 'react-native-reanimated';
import { Text, View } from 'react-native';
const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const AuthStack = createStackNavigator();
const EmpresaStack = createStackNavigator();

export function DrawerScreens({navigation, style}) {
  return (
    <Animated.View  style={[{flex:1,overflow:'hidden'},style]}>
   
      <Stack.Navigator
        initialRouteName="HomeStack"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="HomeStack" component={HomeScreens} />
        <Stack.Screen name="EmpresaStack" component={EmpresaScreens} />
        <Stack.Screen name="Profile" component={ProfileScreen} />

        <Stack.Screen name="configApp" component={configApp} />
      </Stack.Navigator>
   </Animated.View>
  );
}
export const EmpresaScreens = () => (
  <EmpresaStack.Navigator
    initialRouteName="Empresa"
    screenOptions={{
      headerShown: false,
    }}>
    <EmpresaStack.Screen name="Empresa" component={EmpresaScreen} />
    <EmpresaStack.Screen name="DespoCad" component={DespoCad} />
    <EmpresaStack.Screen name="configEmpresa" component={configEmpresa} />
  </EmpresaStack.Navigator>
);
export const HomeScreens = () => (
  <HomeStack.Navigator
    initialRouteName="DespoHome"
    screenOptions={{
      headerShown: false,
    }}>
    <HomeStack.Screen name="DespoHome" component={DespoHome} />
    <HomeStack.Screen name="DespoResumo" component={DespoResumo} />
    <HomeStack.Screen name="DespoSaldos" component={DespoSaldos} />
    <HomeStack.Screen name="DespoRs" component={DespoRs} />
  </HomeStack.Navigator>
);
export const AuthScreens = () => (
  <AuthStack.Navigator
    initialRouteName="Login"
    screenOptions={{
      headerShown: false,
    }}>
    <AuthStack.Screen name="Login" component={LoginScreen} />
    <AuthStack.Screen name="Register" component={cadastroScreen} />
    <AuthStack.Screen name="Recovery" component={RecoveryScreen} />
  </AuthStack.Navigator>
);
